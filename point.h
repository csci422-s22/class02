#ifndef POINT_H_
#define POINT_H_

class Point {

public:
  Point( const double x , double y , double z );
  Point( const double* x );

  double  x() const { return x_; }
  double& x()       { return x_; }
  double  y() const { return y_; }
  double& y()       { return y_; }
  double  z() const { return z_; }
  double& z()       { return z_; }

  double  x( int d ) const;
  double& x( int d );

private:
  double x_, y_, z_;
};

#endif
