default:
	g++ -std=c++11 -c point.cpp -o point.o
	g++ -std=c++11 main.cpp -o program point.o

clean:
	rm -rf program *.o
